import hashlib
import string

# 7 8 12
flag = "leoctf{..E55.E}"
d = string.ascii_uppercase

for i in d:
    flag = flag[0:7] + i + flag[7 + 1 :]
    for i in d:
        flag = flag[0:8] + i + flag[8 + 1 :]
        for i in d:
            str = flag[0:12] + i + flag[12 + 1 :]
            hash = hashlib.md5(str.encode("utf8"))
            if hash.hexdigest() == "5342ac4f509e9a34397e81e9634d079b":
                print(str) # leoctf{GUE55ME}
