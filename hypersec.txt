1. Alice -> HyperSec: HELLO, Alice, HyperSec
2. Alice <- HyperSec: 93327
3. Alice -> HyperSec: AUTHN, cc5764b2fdee7943d10961e1aab6f77f96d84070, b5841701a9c8b70461ab8b51d4b1322aeb2ac3d7
4. Alice <- HyperSec: ...............


I send:

+ HELLO, Alice, HyperSec
+ Server answered with nonce 66028
+ AUTHN, 3833af21626b7e3fa9e9a480bcf8ac7209d34ea9, b5841701a9c8b70461ab8b51d4b1322aeb2ac3d7
+ Server answered with flag leoctf(HyperSecIsJustHype) (format is wrong but IDC)

mind that sha1sum(66028)=3833af21626b7e3fa9e9a480bcf8ac7209d34ea9